package com.example.business

import com.example.business.mapper.toNetwork
import com.example.business.model.Enterprise
import com.example.business.model.User
import com.example.network.NetworkContract
import com.example.network.api.ApiWorker
import io.reactivex.Completable
import io.reactivex.Single

class BusinessWorker : BusinessContract {

    val contract: NetworkContract = ApiWorker()

    override fun sigIn(user: User): Completable {
        return Completable.create {
            contract.sigIn(user.toNetwork())
        }
    }

    override fun getEnterprise(enterprisesType: Int, name: String): Single<List<Enterprise>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEnterpriseById(id: Int): Single<Enterprise> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEnterpriseWithFilter(type: Int, name: String): Single<List<Enterprise>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}