package com.example.business.mapper

import com.example.business.model.EnterpriseType
import com.example.network.model.EnterpriseTypeNetworkModel

fun EnterpriseTypeNetworkModel.toModel(): EnterpriseType {
    return EnterpriseType(
            this.id,
            this.enterpriseTypeName
    )
}