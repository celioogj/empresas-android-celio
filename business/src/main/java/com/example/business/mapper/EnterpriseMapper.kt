package com.example.business.mapper

import com.example.business.model.Enterprise
import com.example.network.model.EnterpriseNetworkModel

fun EnterpriseNetworkModel.toModel(): Enterprise {
    return Enterprise(
            this.id,
            this.enterpriseName,
            this.enterpriseDescription,
            this.emailEnterprise,
            this.facebook,
            this.twitter,
            this.linkedIn,
            this.phone,
            this.ownEnterprise,
            this.photo,
            this.value,
            this.ownShares,
            this.sharePrice,
            this.ownShares,
            this.city,
            this.country,
            this.type.toModel(),
            this.success
    )
}

fun List<EnterpriseNetworkModel>.toModelList(): List<Enterprise> {
    return this.map {
        it.toModel()
    }
}