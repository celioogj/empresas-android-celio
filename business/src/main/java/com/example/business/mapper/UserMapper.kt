package com.example.business.mapper

import com.example.business.model.User
import com.example.network.model.UserNetworkModel

fun UserNetworkModel.toModel(): User {
    return User(
            this.email,
            this.password
    )
}

fun User.toNetwork(): UserNetworkModel {
    return UserNetworkModel(
            this.email,
            this.password
    )
}