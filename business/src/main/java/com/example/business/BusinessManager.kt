package com.example.business

import android.content.Context
import com.example.network.NetworkManager

class BusinessManager {

    companion object {
        fun start(context: Context) {
            NetworkManager.start(context)
        }
    }
}