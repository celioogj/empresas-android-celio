package com.example.business

import com.example.business.model.Enterprise
import com.example.business.model.User
import io.reactivex.Completable
import io.reactivex.Single

interface BusinessContract {

    fun sigIn(user: User): Completable

    fun getEnterprise(enterprisesType: Int, name: String): Single<List<Enterprise>>

    fun getEnterpriseById(id: Int): Single<Enterprise>

    fun getEnterpriseWithFilter(type: Int, name: String): Single<List<Enterprise>>
}