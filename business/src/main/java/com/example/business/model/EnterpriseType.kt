package com.example.business.model

data class EnterpriseType(
        var id: Long = 0,
        var enterpriseTypeName: String = "")