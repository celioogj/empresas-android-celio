package com.example.business.model

data class User(
        var email: String = "",
        var password: String = ""
)