package com.example.business.model

data class Enterprise(
        var id: Long = 0,
        var enterpriseName: String = "",
        var enterpriseDescription: String = "",
        var emailEnterprise: String = "",
        var facebook: String = "",
        var twitter: String = "",
        var linkedIn: String = "",
        var phone: String = "",
        var ownEnterprise: Boolean = false,
        var photo: String = "",
        var value: Long = 0,
        var shares: Int = 0,
        var sharePrice: Long = 0,
        var ownShares: Int = 0,
        var city: String = "",
        var country: String = "",
        var type: EnterpriseType = EnterpriseType(),
        var success: Boolean = false
)