![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias sobre o teste feito.

### VCS ###

O controle de versao foi feito no Git e utilizado o padrao de commit Conventional Commit.

### Arquitetura ###

O projeto foi dividido em modulos independentes e que comunicam atraves de interfaces com os metodos previamente definidos.
Desta forma, aumenta o desacoplamento do mesmo e torna o CORE do app independente das tecnologias utlizada na cada de Network e de Persistencia.

### Design Patter ###

A escolha feita foi o MVP, por ser amplamente utilizada definir contratos para cada view.

### Dependencias ###

As dependencias foram separadas, para evitar redundancia e download desnecessario a cada modulo. Por exemplo, a camada de persistencia ter a lib do Retrofit, sendo que e utilizado na cada de network.

* RxKotlin, RxAndroid, RxJava: tornar o codigo reativo e melhorar o mapeamento de error;
* Retrofit e OkHttp: modernos e de facil compreensao.
* Picasso: gerenciar download e por familiaridade.

### Em caso de prazo maior ###

* Primeiramente eu iria terminar o projeto, mas final de semestre letivo sempre e um pesadelo.
* Eu teria inserido tambem alguns testes unitarios simples, mesmo que nao seja um ponto meu de melhoria.
* Teria utlizado o Glide, como oportunidade de aprendizado.
* Independente do termino, acredito que consegui expor alguns dos meus conhecimentos com a plataforma Android, a linguagem Kotlin e as bibliotecas utlizadas no projeto.

### Execucao do App ###

Infelizmente nao e possivel executa-lo pois nao o conclui. Para avaliacao do mesmo, favor consultar a brach master.