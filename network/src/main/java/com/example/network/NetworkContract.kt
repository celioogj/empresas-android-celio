package com.example.network

import com.example.network.model.EnterpriseNetworkModel
import com.example.network.model.UserNetworkModel
import io.reactivex.Completable
import io.reactivex.Single

interface NetworkContract {

    fun sigIn(user: UserNetworkModel): Completable

    fun getEnterprise(enterprisesType: Int, name: String): Single<List<EnterpriseNetworkModel>>

    fun getEnterpriseById(id: Int): Single<EnterpriseNetworkModel>

    fun getEnterpriseWithFilter(type: Int, name: String): Single<List<EnterpriseNetworkModel>>
}