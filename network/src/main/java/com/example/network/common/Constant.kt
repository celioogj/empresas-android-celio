package com.example.network.common

internal object Constant {

    const val TOKEN: String = "token"

    //header constants
    const val ACCESS_TOKEN: String ="access-token"
    const val CLIENT: String = "client"
    const val UID: String = "uid"

}