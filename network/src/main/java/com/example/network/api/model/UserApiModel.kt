package com.example.network.api.model

internal data class UserApiModel(
        var email: String = "",
        var password: String = ""
)