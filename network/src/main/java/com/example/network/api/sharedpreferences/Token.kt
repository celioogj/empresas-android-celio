package com.example.network.api.sharedpreferences

data class Token(
        var accessToken: String = "",
        var client: String = "",
        var uid: String = ""
)