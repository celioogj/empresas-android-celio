package com.example.network.api.mapper

import com.example.network.api.model.UserApiModel
import com.example.network.model.UserNetworkModel

internal fun UserApiModel.toNetwork(): UserNetworkModel {
    return UserNetworkModel(
            this.email,
            this.password
    )
}

internal fun UserNetworkModel.toApi(): UserApiModel {
    return UserApiModel(
            this.email,
            this.password
    )
}