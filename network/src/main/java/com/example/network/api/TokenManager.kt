package com.example.network.api

import com.example.network.NetworkManager
import com.example.network.api.sharedpreferences.Token
import com.example.network.common.Constant
import com.google.gson.Gson

class TokenManager {

    fun get(): Token? {
        return NetworkManager.sharedPreferences.getString(Constant.TOKEN, null)
                ?.let { Gson().fromJson(it, Token::class.java) }
    }

    fun save(token: Token): Token? {
        NetworkManager.sharedPreferences.edit().putString(Constant.TOKEN, Gson().toJson(token))
                .apply()
        return get()
    }

    fun delete() {
        NetworkManager.sharedPreferences.edit().remove(Constant.TOKEN).apply()
    }
}