package com.example.network.api.model

import com.google.gson.annotations.SerializedName

internal data class EnterpriseApiModel(
        var id: Long = 0,
        @SerializedName("enterprise_name")
    var enterpriseName: String = "",
        var enterpriseDescription: String = "",
        @SerializedName("email_enterprise")
    var emailEnterprise: String = "",
        var facebook: String = "",
        var twitter: String = "",
        var linkedIn: String = "",
        var phone: String = "",
        @SerializedName("own_enterprise")
    var ownEnterprise: Boolean = false,
        var photo: String = "",
        var value: Long = 0,
        var shares: Int = 0,
        @SerializedName("share_price")
    var sharePrice: Long = 0,
        @SerializedName( "own_shares")
    var ownShares: Int = 0,
        var city: String = "",
        var country: String = "",
        var type: EnterpriseTypeApiModel = EnterpriseTypeApiModel(),
        var success: Boolean = false
)