package com.example.network.api.sharedpreferences

import com.example.network.api.model.TokenApiModel

internal fun TokenApiModel.toModel(): Token {
    return Token(
            this.accessToken,
            this.client,
            this.uid
    )
}

internal fun Token.toApi(): TokenApiModel {
    return TokenApiModel(
            this.accessToken,
            this.client,
            this.uid
    )
}