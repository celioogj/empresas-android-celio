package com.example.network.api.model

import com.google.gson.annotations.SerializedName

internal data class TokenApiModel(
        @SerializedName("access-token")
        var accessToken: String = "",
        var client: String = "",
        var uid: String = ""
)