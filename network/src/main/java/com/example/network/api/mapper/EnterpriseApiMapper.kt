package com.example.network.api.mapper

import com.example.network.api.model.EnterpriseApiModel
import com.example.network.model.EnterpriseNetworkModel

internal fun EnterpriseApiModel.toNetwork(): EnterpriseNetworkModel {
    return EnterpriseNetworkModel(
            this.id,
            this.enterpriseName,
            this.enterpriseDescription,
            this.emailEnterprise,
            this.facebook,
            this.twitter,
            this.linkedIn,
            this.phone,
            this.ownEnterprise,
            this.photo,
            this.value,
            this.ownShares,
            this.sharePrice,
            this.ownShares,
            this.city,
            this.country,
            this.type.toNetwork(),
            this.success
    )
}

internal fun List<EnterpriseApiModel>.toNetworkList(): List<EnterpriseNetworkModel> {
    return this.map {
        it.toNetwork()
    }
}