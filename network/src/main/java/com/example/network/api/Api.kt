package com.example.network.api

import com.example.network.NetworkManager
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

internal object Api {

    private const val URL_BASE: String = "http://empresas.ioasys.com.br/v1"
    private const val DEFAULT_DATE_FORMAT = "dd/MM/yyyy"

    private var retrofit = Retrofit.Builder()
            .baseUrl(URL_BASE)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setDateFormat(DEFAULT_DATE_FORMAT).create()))
            .client(NetworkManager.okHttpClient)
            .build()

    internal val routes = retrofit.create(Route::class.java)

}