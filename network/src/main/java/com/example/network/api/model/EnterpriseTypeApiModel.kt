package com.example.network.api.model

import com.google.gson.annotations.SerializedName

internal data class EnterpriseTypeApiModel(
        var id: Long = 0,
        @SerializedName("enterprise_type_name")
        var enterpriseTypeName: String = ""
)