package com.example.network.api

import com.example.network.NetworkContract
import com.example.network.api.mapper.toApi
import com.example.network.api.sharedpreferences.toModel
import com.example.network.model.EnterpriseNetworkModel
import com.example.network.model.UserNetworkModel
import io.reactivex.Completable
import io.reactivex.Single

class ApiWorker : NetworkContract {

    override fun sigIn(user: UserNetworkModel): Completable {
        return Completable.create {
            Api.routes.signIn(user.toApi())
                    .doOnSuccess {
                        TokenManager().save(it.toModel())
                    }
        }
    }

    override fun getEnterprise(enterprisesType: Int, name: String): Single<List<EnterpriseNetworkModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEnterpriseById(id: Int): Single<EnterpriseNetworkModel> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEnterpriseWithFilter(type: Int, name: String): Single<List<EnterpriseNetworkModel>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}