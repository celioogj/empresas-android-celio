package com.example.network.api

import com.example.network.api.model.EnterpriseApiModel
import com.example.network.api.model.TokenApiModel
import com.example.network.api.model.UserApiModel
import io.reactivex.Single
import retrofit2.http.*

internal interface Route {

    @POST("/users/auth/sign_in")
    fun signIn(@Body userApiModel: UserApiModel) : Single<TokenApiModel>

    @GET("/enterprises")
    fun getEnterprise(@Query("enterprise_types") enterprisesType: Int, @Query("name") name: String): List<EnterpriseApiModel>

    @GET("enterprises/{id}")
    fun getEnterpriseById(@Path("id") id: Long): Single<EnterpriseApiModel>

    @GET("enterprises")
    fun getEnterpriseWithFilter(@Query("enterprise_types") type: Int, @Query("name") name: String): Single<List<EnterpriseApiModel>>
}


