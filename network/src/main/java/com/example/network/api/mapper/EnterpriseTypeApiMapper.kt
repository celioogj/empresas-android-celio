package com.example.network.api.mapper

import com.example.network.api.model.EnterpriseTypeApiModel
import com.example.network.model.EnterpriseTypeNetworkModel

internal fun EnterpriseTypeApiModel.toNetwork(): EnterpriseTypeNetworkModel {
    return EnterpriseTypeNetworkModel(
            this.id,
            this.enterpriseTypeName
    )
}

internal fun EnterpriseTypeNetworkModel.toApi(): EnterpriseTypeApiModel {
    return EnterpriseTypeApiModel(
            this.id,
            this.enterpriseTypeName
    )
}