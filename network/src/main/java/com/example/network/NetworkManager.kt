package com.example.network

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.network.api.TokenManager
import com.example.network.api.sharedpreferences.Token
import com.example.network.common.Constant
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request

class NetworkManager {

    companion object {
        @Volatile
        private var instance: NetworkManager? = null
        internal lateinit var sharedPreferences: SharedPreferences
        internal lateinit var okHttpClient: OkHttpClient

        fun start(context: Context) {
            okHttpClient = getOkHttpClient(context)
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        }

        private fun getOkHttpClient(context: Context): OkHttpClient {
            val builder: OkHttpClient.Builder = OkHttpClient.Builder()
            builder.interceptors().add(Interceptor { chain ->
                val newBuilder: Request.Builder = chain.request().newBuilder()

                var token: Token? = TokenManager().get()
                newBuilder.addHeader("Content-Type", "application/x-www-form-urlencoded")
                newBuilder.addHeader(Constant.ACCESS_TOKEN, token?.accessToken)
                newBuilder.addHeader(Constant.CLIENT, token?.client)
                newBuilder.addHeader(Constant.UID, token?.uid)

                chain.proceed(newBuilder.build())
            })
            return builder.build()
        }
    }

}