package com.example.network.model

data class UserNetworkModel(
        var email: String = "",
        var password: String = ""
)