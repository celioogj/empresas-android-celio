package com.example.network.model

data class EnterpriseTypeNetworkModel(
        var id: Long = 0,
        var enterpriseTypeName: String = ""
)