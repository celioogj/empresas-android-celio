package com.example.app.application

import android.app.Application
import com.example.business.BusinessManager

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        initBusinessManager()
    }

    private fun initBusinessManager() {
        BusinessManager.start(this)
    }
}