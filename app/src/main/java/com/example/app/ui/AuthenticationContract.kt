package com.example.app.ui

interface AuthenticationContract {

    interface View {

        fun showLoading()
        fun hideLoading()
        fun signInSuccess()
        fun signINFail(message: String)

    }

    interface Presenter {

        fun signIn(email: String, password: String)

    }
}